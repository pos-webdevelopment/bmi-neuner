<?php
$error = [];
function validateData($name, $datum, $height, $weight, $gender)
{
    return validateName($name) & validateHeight($height) & validateWeight($weight) & validateDate($datum) & validateGender($gender);
}

function validateName($name) {
    global $error;
    if (strlen($name) == 0) {
        $error[] = "Bitte geben Sie einen Namen an!";
        return false;
    } elseif (strlen($name) > 25) {
        $error[] = "Der Name ist zu lang.";
        return false;
    } else {
        return true;
    }
}

function validateHeight($height) {
    global $error;
    if ($height == 0) {
        $error[] = "Bitte geben Sie einen Körpergröße an!";
        return false;
    } elseif ($height > 250) {
        $error[] = "Die angegebene Größe ist zu groß ... ";
        return false;
    } elseif ($height < 50) {
        $error[] = "Die angegebene Größe ist zu klein ... ";
        return false;
    } else {
        return true;
    }
}

function validateWeight($weight) {
    global $error;
    if ($weight == 0) {
        $error[] = "Bitte geben Sie ein Gewicht an!";
        return false;
    } elseif ($weight > 250) {
        $error[] = "Das angegebene Gewicht ist zu groß ... ";
        return false;
    } elseif ($weight < 25) {
        $error[] = "Das angegebene Gewicht ist zu klein ... ";
        return false;
    } else {
        return true;
    }
}

function validateGender($gender) {
    global $error;
    if($gender != 'male' && $gender != 'female' && $gender != 'other') {
        $error['gender'] = "Es wurde kein Geschlecht ausgewählt.";
        return false;
    } else {
        return true;
    }
}

function validateDate($date) {
    global $error;
    try {
        if ($date == '') {
            $error['datum'] = "Datum darf nicht leer sein!";
            return false;
        } else if (new DateTime($date) > new DateTime()) {
            $error['datum'] = "Datum darf nicht in der Zukunft liegen!";
            return false;
        } else {
            return true;
        }
    } catch (Exception $e) {
        $error['datum'] = "Datum ist ungültig!";
        return false;
    }
}

function calcBMI($weigth, $height, $gender) {
    if($height != 0) { // no division by ZERO
        $bmi = $weigth / (pow(($height / 100), 2));
        $bmiValue = "Dein persönlicher BMI-Wert beträgt " . round($bmi, 1) . ".";
        if($gender=="male") {
            // values for men
            if ($bmi < 20) {
                echo "<p class='alert alert-warning'>Du bist untergewichtig!<br> " . $bmiValue . "</p>";
            } elseif ($bmi < 24.9) {
                echo "<p class='alert alert-success'>Du hast Normalgewicht! Sehr schön :D<br> " . $bmiValue . "</p>";
            } elseif ($bmi < 29.9) {
                echo "<p class='alert alert-info'>Du bist übergewichtig!<br> " . $bmiValue . "</p>";
            } else {
                echo "<p class='alert alert-danger'>Du hast Adipositas.<br> " . $bmiValue . "</p>";
            }
        } else {
            // values for women
            if ($bmi < 18) {
                echo "<p class='alert alert-warning'>Du bist untergewichtig!<br> " . $bmiValue . "</p>";
            } elseif ($bmi < 23.9) {
                echo "<p class='alert alert-success'>Du hast Normalgewicht! Sehr schön :D<br> " . $bmiValue . "</p>";
            } elseif ($bmi < 27.9) {
                echo "<p class='alert alert-info'>Du bist übergewichtig!<br> " . $bmiValue . "</p>";
            } else {
                echo "<p class='alert alert-danger'>Du hast Adipositas.<br> " . $bmiValue . "</p>";
            }
        }
    }
}
?>