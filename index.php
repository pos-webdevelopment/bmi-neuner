<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/validation.js"></script>
    <title>BMI-Calculator</title>
</head>
<body>
<div class="container">
    <h1 class="mt-5 mb-3">Body-Mass-Index-Rechner</h1>

    <?php
    require "lib/func.inc.php";
    $name = $datum = $height = $weight = $gender = '';
    if (isset($_POST['submit'])) {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $datum = isset($_POST['datum']) ? $_POST['datum'] : '';
        $weight = isset($_POST['weight']) ? $_POST['weight'] : '';
        $height = isset($_POST['height']) ? $_POST['height'] : '';
        $gender = isset($_POST['gender']) ? $_POST['gender'] : '';

        if (validateData($name, $datum, $height, $weight, $gender)) {
            //echo "<p class='alert alert-success'>Die eingegebenen Daten sind korrekt. :)</p>";
            calcBMI($weight, $height, $gender);
        } else {
            echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft. :(</p><ul>";

            foreach ($error as $k => $value) { // $errors-Variable nicht bekannt, wurde allerdings durch require eingebunden, daher i.O.
                echo "<li>" . $value . "</li>";
            }
            echo "</ul></div>";
        }
    }
    ?>

    <form action="index.php" method="post">
        <div class="col-sm-4 float-right">
            <h2>Info zum BMI</h2>
            <p>Unter 18,5 Untergewicht</p>
            <p>18,5 - 24,9 Normalgewicht</p>
            <p>25,0 - 29,9 Übergewicht</p>
            <p>30,0 und darüber Adipositas</p>
        </div>
        <div class="row">
            <div class="col-sm-8 form-group">
                <label for="name">Name*</label>
                <input type="text"
                       name="name"
                       class="form-control <?= isset($error['name']) ? 'is-invalid' : '' ?>"
                       maxlength="25"
                       required
                       value="<?= htmlspecialchars($name) ?>">
            </div>
            <div class="col-sm-4 form-group">
                <label for="datum">Messdatum*</label>
                <input type="date"
                       name="datum"
                       class="form-control <?= isset($error['datum']) ? 'is-invalid' : '' ?>"
                       required
                       onchange="validateDate(this)"
                       value="<?= htmlspecialchars($datum) ?>">

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label for="height">Größe (cm)*</label>
                <input type="number"
                       name="height"
                       class="form-control <?= isset($error['height']) ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars($height) ?>"
                       min="50"
                       max="250"
                       required>
            </div>
            <div class="col-sm-6">
                <label for="weight">Gewicht (kg)*</label>
                <input type="number"
                       name="weight"
                       class="form-control <?= isset($error['weight']) ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars($weight) ?>"
                       min="25"
                       max="250"
                       required>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-6 form-group">
                <li style="list-style: none">
                    <div>
                        <label for="gender" id="gender">Geschlecht*</label>
                        <input type="radio"
                               name="gender"
                               id="male"
                               class="<?= isset($error['gender']) ? 'is-invalid' : '' ?>"
                               value="male" <?php if ($gender == 'male') echo "checked='checked'" ?>
                               required/>
                        <label for="male">männlich</label>

                        <input type="radio"
                               name="gender"
                               id="female"
                               class="<?= isset($error['gender']) ? 'is-invalid' : '' ?>"
                               value="female" <?php if ($gender == 'female') echo "checked='checked'" ?>
                               required/>
                        <label for="female">weiblich</label>

                        <input type="radio"
                               name="gender"
                               id="other"
                               class="<?= isset($error['gender']) ? 'is-invalid' : '' ?>"
                               value="other" <?php if ($gender == 'other') echo "checked='checked'" ?>
                                required/>
                        <label for="other">divers</label>
                    </div>
                </li>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <button name="submit" value="submit" type="submit" class="btn btn-block btn-primary">Speichern</button>
            </div>
            <div class="col-sm-3">
                <a href="index.php" class="btn btn-block btn-secondary">Löschen</a>
            </div>
        </div>


    </form>
</div> <!-- container -->

</body>
</html>

